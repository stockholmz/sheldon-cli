# Sheldon-cli
PHP based CLI tool to manage your lots of SSH connections.

N.B. Using passwords for SSH requires package sshpass.
### Dependencies
* PHP 5.6+

### Installation
Open your terminal and go to where you placed Sheldon.
```sh
$ sudo ln -s sheldon.php /usr/local/bin/sheldon
```
### Usage
To see a list of commands available use:
```sh
$ sheldon
```
To add one of your many SSH connections to your list:
```sh
$ sheldon add
```
And just follow the instructions. Shit aint rocket science.

To see your list of connections:
```sh
$ sheldon show
```
And just go with the flow. Choose your connection and get work done.

To remove a connection:
```sh
$ sheldon remove
```
Yet again. This. Is. Not. Rocket. Science.
