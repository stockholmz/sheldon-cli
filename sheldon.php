#!/usr/bin/php
<?php
/**
 * Sheldon-cli is a SSH easing tool, to manage the lots of your daily connections.
 * https://gitlab.com/stockholmz/sheldon-cli
 *
 * Shortcut should be made in /usr/local/bin/sheldon.
 * Example "ln -s #THIS_FILE# /usr/local/bin/sheldon".
 * It CAN be nessecary to provide the symlink further permissions.
 *
 * @author Nikolaj Stockholm <kontakt@nikolajstockholm.dk>
 */
require_once('colors.php');

/**
 * Sheldon-cli.
 */
class Sheldon {
  var $arguments = array();
  var $list_path = '';
  var $directory = '';
  var $commands = array(
    'help',
    '--help',
    'show',
    'add',
    'remove',
  );

  /**
   * Constructor, holding functionality for each command.
   *
   * @param array $arguments
   */
  function __construct($arguments = array()) {
    $this->clear();

    $this->arguments = $arguments;

    $arg1 = $this->arguments[1];
    $arg1 = str_replace('-', '', $arg1);

    if(empty($arg1)) {
      $arg1 = 'show';
    }

    if (in_array($arg1, $this->commands)) {
      $this->{$arg1}();
    }
    else {
      $this->help();
    }
  }

  /**
   * Add connection to list, and interacts with user input.
   */
  private function add() {
    EnterTitle:
      print 'Enter Title (server name): ';
      $title = $this->get_input();
      if (!empty($title)) {
          goto EnterHost;
      }
      else {
        goto EnterTitle;
      }
    EnterHost:
      print 'Enter hostname: ';
      $hostname = $this->get_input();
      if (!empty($hostname)) {
        goto EnterUsername;
      }
      else {
        goto EnterHost;
      }

    EnterUsername:
      print 'Enter username: ';
      $username = $this->get_input();
      if (!empty($username)) {
        goto EnterPassword;
      }
      else {
        goto EnterUsername;
      }

    EnterPassword:
      print 'Enter password: ';
      $password = $this->get_input();

      $this->_set_list(array(
        $title => array(
          'host' => $hostname,
          'username' => $username,
          'password' => $password,
        ),
      ));
      die();

    goto EnterHost;
  }

  /**
   * Internal function to get list from json file.
   *
   * @return array
   */
  private function _get_list() {
    $file_path = explode('/', __FILE__);
    array_pop($file_path);

    $this->directory = implode('/', $file_path);

    $file_path = implode('/', $file_path) . '/sheldon-list.json';

    $this->list_path = $file_path;

    if ($file = @file_get_contents($file_path)) {
      $file = json_decode($file, TRUE);
    }
    elseif (touch($file_path)) {
      $file = array();
    }
    else {
      $this->log_status('Could not access or create settings file. Please check file permissions.');
      die();
    }

    return !empty($file) ? $file : array();
  }

    /**
     * Internal function to append row to json file.
     *
     * @param array $data
     *   hostname => ('username' => #username#, 'password' => #password#).
     * @return bool
     */
  private function _set_list($data = array(), $merge = TRUE) {
    if ($merge) {
      $list = $this->_get_list();
      $list = array_merge($list, $data);
    }
    else {
      $list = $data;
    }
    $list = json_encode($list);

    if (file_put_contents($this->list_path, $list)) {
      $this->log_status('List updated', TRUE);
      return TRUE;
    }
    else {
      $this->log_status('List not updated');
      return FALSE;
    }
    return FALSE;
  }

  private function print_list($list = array()) {
    $list = !empty($list) ? $list : $this->_get_list();
    $this->log('You have ' . count($list) . ' connections available.');
    $this->log("ID\t|\tServer Title\n");
    $i = 0;
    foreach ($list as $key => $value) {
      $this->log($i . "\t|\t" . $key);
      $i++;
    }
  }

  /**
   * Shows the list of connections, interacts with user input and removes chosen.
   */
  private function remove() {
    ListHosts:
      $list = $this->_get_list();

      if (!empty($list)) {
        $this->print_list($list);
        print 'Choose host to delete: ';
        $input = $this->get_input();
        if (isset($input) && is_numeric($input) && $input <= count($list) - 1) {
          $key_list = array_keys($list);
          goto RemoveHost;
        }
        elseif (isset($input) && $input == 'exit') {
          die();
        }
        else {
          $this->clear();
          unset($input);
          goto ListHosts;
        }
      }
      else {
        $this->log('No connections available. Type "sheldon add" to add connections to your list.');
      }

    RemoveHost:
      unset($list[$key_list[$input]]);
      if ($this->_set_list($list, FALSE)) {
        $this->log_status($key_list[$input] . ' removed!', TRUE);
        die();
      }
      else {
        $this->log_status($key_list[$input] . ' not removed!');
        die();
      }


    goto ListHosts;
  }

  /**
   * Shows the list of connections, and interacts with user input.
   */
  private function show() {
    ListHosts:
      $list = $this->_get_list();

      if (!empty($list)) {
        $this->print_list($list);
        print 'Choose host: ';
        $input = $this->get_input();
        if (isset($input) && is_numeric($input) && $input <= count($list) - 1) {
          $key_list = array_keys($list);
          $server = $list[$key_list[$input]];
          goto ConnectSSH;
        }
        elseif (isset($input) && $input == 'exit') {
          die();
        }
        else {
          $this->clear();
          unset($input);
          goto ListHosts;
        }
      }
      else {
        $this->log('No connections available. Type "sheldon add" to add connections to your list.');
      }

    ConnectSSH:
      $this->clear();
      $this->log('Connecting to: ' . $key_list[$input] . '...');
      $this->ssh($server['username'], $server['host'], $server['password']);


    goto ListHosts;
  }

  /**
   * Parses chosen connection info to expect script.
   *
   * @param string $hostname
   * @param string $username
   * @param string $password
   *
   * @return application
   *   Opens SSH connection or throws list of connections.
   */
  private function ssh($username, $hostname, $password = NULL) {
    if ($password) {
      passthru('sshpass -p "' . $password . '" ssh -o StrictHostKeyChecking=no ' . $username . '@' . $hostname);
    }
    else {
      passthru('ssh ' . $username . '@' . $hostname);
    }
  }

  /**
   * Returns user input from terminal.
   *
   * @return string
   */
  private function get_input() {
    $fp = @fopen("php://stdin", "r");
    $str = fread($fp, 1024);
    fclose($fp);
    $sn = ((PHP_SHLIB_SUFFIX == "dll")?2:1);
    return substr($str, 0, (strlen($str) - $sn));
  }

  /**
   * Shows a list of commands available.
   */
  private function help() {
    if (!empty($this->commands) && is_array($this->commands)) {
      $this->log('The following commands are available to use:');
      $this->log("\t".' - Use "sheldon COMMAND --help" for description regarding said command.');

      foreach ($this->commands as $key => $value) {
        $this->log("\t" . $value, 'light_blue');
      }
    }
    else {
      $this->log_status('No commands found');
    }
  }

  /**
   * Wrapper function to throw clear command to terminal.
   * For clearing out old text.
   */
  private function clear() {
    passthru('clear');
  }

  /**
   * Prints message, optionally in colors.
   *
   * @param string $message
   * @param mixed $string_color
   *   takes a human-readalbe color, refered in ColorCLI.
   * @param mixed $background_color
   *   takes a human-readalbe color, refered in ColorCLI.
   *
   * @see ColorCLI
   *
   * @return string
   */
  private function log($message = '', $string_color = NULL, $background_color = NULL) {
    print ColorCLI::getColoredString($message, $string_color, $background_color);
    print "\n";
  }

  /**
   * Prints message + OK/Error in appropriate color.
   *
   * @param string $message
   * @param bool $status
   *
   * @return string
   */
  private function log_status($message = '', $status = FALSE) {
    print $message;
    print ' - ' . ($status ? ColorCLI::getColoredString('OK', 'green') : ColorCLI::getColoredString('Error', 'red'));
    print "\n";
  }

  private function beep() {
    print "\x07";
  }
}

/**
 * Just make it nice upon shutdown.
 */
register_shutdown_function(function() {
  print "\n\n";
});

$arguments = (!empty($argv[1]) ? $argv : array());
$cli = new Sheldon($arguments);
